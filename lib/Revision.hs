module Revision (getRevTH)
where

import Data.Char (isSpace)
import Data.List (dropWhileEnd)
import Language.Haskell.TH (ExpQ, runIO, stringE)
import System.Process (readProcessWithExitCode)

getRev :: IO String
getRev = either failed getOutput <$> try (readProcessWithExitCode "get-rev" [] "")
  where
    failed (_ :: IOError)    = "UNKNOWN"
    getOutput (_, stdout, _) = dropWhileEnd isSpace stdout

getRevTH :: ExpQ
getRevTH = stringE =<< runIO getRev

