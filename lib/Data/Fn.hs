module Data.Fn where

-- same fixity as (.)
infixr 9 .-.
(.-.) :: (b -> c) -> (a -> a1 -> b) -> a -> a1 -> c
(.-.) = (.).(.)
