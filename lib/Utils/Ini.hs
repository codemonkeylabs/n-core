module Utils.Ini where

import Data.HashMap.Strict as HM  (lookup)
import Data.Function              (on)
import Data.Ini                   (Ini(..), readIniFile)
import Data.List                  (nubBy)

parseIniConfig :: Text -> Ini -> [(Text, Text)]
parseIniConfig section Ini {..} =
  let kvs = fromMaybe [] $ HM.lookup section iniSections
  in fmap parseField <$> kvs
    where parseField "yes"   = true
          parseField "true"  = true
          parseField "no"    = false
          parseField "false" = false
          parseField str     = str
          true               = pack $ show True
          false              = pack $ show False

getIniOpts :: MonadIO m => Text -> FilePath -> [(Text, Text)] -> m [(Text, Text)]
getIniOpts sectionName iniPath defaultConfig =
  maybeDefaultConfig . runMaybeT $ do
    iniData <- hoist (const ()) =<< failableIO (readIniFile iniPath)
    -- this "merges" the default config options with the ones from the ini file,
    -- giving preference to the configuration found in it.
    return . withoutDups $ parseIniConfig sectionName iniData <> defaultConfig
      where withoutDups        = nubBy ((==) `on` fst)
            maybeDefaultConfig = fmap $ fromMaybe defaultConfig
